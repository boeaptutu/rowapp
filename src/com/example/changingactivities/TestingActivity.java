package com.example.changingactivities;

import java.util.Random;

import nl.tudelft.rowapp.MainActivity;
import nl.tudelft.dataProcessing.*;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Build;

public class TestingActivity extends Activity {

	private TextView time;
	private TextView force;
	private TextView power;
	private TextView spm;
	
	private short[] dummyForce = new short[17];//{100, 700, 1400, 900, 700, 500, 300, 100, 1300, 1200, 1100, 1000, 900, 800, 700, 600, 500, 400, 300, 200}; //15300 total force
	private short[] dummyAngle = new short[17]; //{30, 40, 50, 70, 80, 90, 100, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0};
	
	private ProcessData processData = new ProcessData();
	
	private double randomNumber = 0;
	private int teller = 0;
	private int dummyTijd = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_real_time_data);
		short x = 0;
		boolean down = false;
		for(int i=0; i<dummyForce.length;i++){
			dummyForce[i] = x;
			if(x==80){
				down = true;
			}
			if(down){
				x-=10;
			}
			else{
				x+=10;
			}
			
		}
		
		//reset parameters
		down = false;
		x = 30;
		for(int i=0; i<dummyAngle.length;i++){
			dummyAngle[i] = x;
//			if(x>=118){
//				down = true;
//			}
//			if(down){
//				x-=8;
//			}
//			else{
//				x+=8;
//			}
			x+=8;
			
		}
		for(int i=0; i<dummyAngle.length;i++){
			Log.d("Dummy","Angle: "+ dummyAngle[i] + "   Force: " + dummyForce[i]);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

	    time = (TextView) findViewById(R.id.Real_Time_Past);
	    force = (TextView) findViewById(R.id.Real_Time_Force);
	    power = (TextView) findViewById(R.id.Real_Time_Power);
	    spm = (TextView) findViewById(R.id.Real_Time_SPM);
	    

	    Thread t = new Thread() {

	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(125); //8Hz
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        	if(teller>3){
	                        		teller = 0;
	                        	}
	                        	incomingData(teller);
	                        	teller++;
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };

	    t.start();
	    
	    
	    Thread print = new Thread() {

	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(1000);
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        	updateTextView();
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };

	    print.start();
	    
	    
	}

	
	private void updateTextView() {
		float powerFloat = processData.displayPower();
		String powerString = String.valueOf(powerFloat);
		power.setText(powerString);
		
		float spmFloat = processData.displaySPM();
		String spmString = String.valueOf(spmFloat);
		spm.setText(spmString);
		
		float forceFloat = processData.displayForce();
		String forceString = String.valueOf(forceFloat);
		force.setText(forceString);
		
		String timePast = processData.DisplayTotalTime();
		time.setText(timePast);
		
	}
	
	private void incomingData(int x){
		for(int i=0; i<dummyAngle.length;i++){
			processData.calculateData(dummyAngle[i], dummyForce[i]);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.testing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_testing,
					container, false);
			return rootView;
		}
	}

}
