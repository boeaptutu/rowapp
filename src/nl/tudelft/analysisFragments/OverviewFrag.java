package nl.tudelft.analysisFragments;

import nl.tudelft.rowapp.MainActivity;
import nl.tudelft.util.TrainingDbAdapter;

import com.example.changingactivities.R;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


// Get info from the database and present that in blocks to the user! (Overview)
public class OverviewFrag extends Fragment {
	private static long _id = 0;
	
	/**
	 * place the new layout (fragment) on the screen
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page2, container, false);
        showData(view);
        return view;
    }
    
    /** 
     * Process the data out of the database and place it on the screen
     * @param view
     */
    private void showData(View view) {
    	// open database
    	MainActivity.mDbHelper.open();
    	
    	// fetch data from database
		Cursor cData = MainActivity.mDbHelper.fetchTrainingData(_id);
		// move to first trainingdata
		
		cData.moveToFirst();
		
		if(cData.getCount() != 0){
			// determine the total time by dividing the the sample frequency; 
			// getCount-1 because the first sample is at 0 seconds so for the total time you should not incorporate that one
			float time = ((float) (cData.getCount() -1) )*MainActivity.SAMPLE_TIME;
			int avrForce = 0;
			
			// Calculate the average force;
			for(int i = 0; i < cData.getCount(); i++){
				avrForce += cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_FORCE));
				cData.moveToNext();
			}
			avrForce /= cData.getCount();
			
			// Place the data in the Overview
	        TextView mTextView = (TextView) view.findViewById(R.id.Fragment2Text1);
	        mTextView.setText(String.valueOf(avrForce));
	
	        mTextView = (TextView) view.findViewById(R.id.Fragment2Text2);
	        mTextView.setText("NaN");
	        
	        mTextView = (TextView) view.findViewById(R.id.Fragment2Text3);
	        mTextView.setText("NaN");
	        
	        mTextView = (TextView) view.findViewById(R.id.Fragment2Text4);
	        mTextView.setText(String.valueOf(time));
	        
	        mTextView = (TextView) view.findViewById(R.id.Fragment2Text5);
	        mTextView.setText("NaN");
	        
	        mTextView = (TextView) view.findViewById(R.id.Fragment2Text6);
	        mTextView.setText("NaN");
		}
		else{
			Log.e("RowTest", "No Data was found!");
		}
	}
   
	/**
     * Initialise new Fragment
     * @param text is used to place text on the screen
     * @return
     */
    public static OverviewFrag newInstance(String text, long trainingId) {
    	_id = trainingId;
        OverviewFrag f = new OverviewFrag();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }    
}