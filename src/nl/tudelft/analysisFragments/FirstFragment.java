package nl.tudelft.analysisFragments;

import nl.tudelft.rowapp.MainActivity;
import nl.tudelft.util.TrainingDbAdapter;

import com.example.changingactivities.R;

import android.support.v4.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

public class FirstFragment extends ListFragment{
	
	private static long _id;
	private Cursor mTrainingData;
	
	/**
	 * place the new layout (fragment) on the screen
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page1, container, false);
		MainActivity.mDbHelper.open();
        fillData();
		MainActivity.mDbHelper.close();
        return view;
    }

    /**
     * Initialise new Fragment
     * @param text is used to place text on the screen, trainingId to get the right data out of the database and mDbH for the database.
     * @return
     */
    public static FirstFragment newInstance(String text, long trainingId) {
    	_id = trainingId;
        FirstFragment f = new FirstFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);
        
        return f;
    }
    
    /**
     * Place all the samples on the screen in a list
     */
	private void fillData() {
		
        // Get all of the notes from the database and create the item list
		mTrainingData = MainActivity.mDbHelper.fetchTrainingData(_id);
        
        String[] fields = new String[] { TrainingDbAdapter.KEY_ID, TrainingDbAdapter.KEY_FORCE, TrainingDbAdapter.KEY_ANGLE, TrainingDbAdapter.KEY_SAMPLENUM };
        int[] view = new int[] { R.id.FragmentText0, R.id.FragmentText1, R.id.FragmentText2 , R.id.FragmentText3 };
        
        // Now create an array adapter and set it to display using our row		
        SimpleCursorAdapter trainingData = new SimpleCursorAdapter(getActivity(), R.layout.training_data, mTrainingData, fields, view);
        setListAdapter(trainingData);
	}
	
}