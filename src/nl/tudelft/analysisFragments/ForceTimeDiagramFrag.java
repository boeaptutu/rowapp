package nl.tudelft.analysisFragments;

import nl.tudelft.rowapp.MainActivity;
import nl.tudelft.util.TrainingDbAdapter;

import com.example.changingactivities.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class ForceTimeDiagramFrag extends Fragment {
	
	private static long _id;
	
	/**
	 * place the new layout (fragment) on the screen
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page3, container, false);

        GraphView forceTimeDiagram;
        forceTimeDiagram = new LineGraphView(
                getActivity() // context
                , "Force-Time Diagram" // heading
        );
        graphSettings(forceTimeDiagram);
        
        getData(forceTimeDiagram);

        LinearLayout layout = (LinearLayout) v.findViewById(R.id.GraphAnalysisFT);
        layout.addView(forceTimeDiagram);

        return v;
    }
    
    /**
     * Get all the data out of the database and place it in the graph
     * @param graph
     */
    private void getData(GraphView graph) {
		try{
	    	// open database
			MainActivity.mDbHelper.open();
			
			// fetch data from database
			Cursor cData = MainActivity.mDbHelper.fetchTrainingData(_id);
			
			cData.moveToLast();
			int numOfPackages = cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM));
			
			GraphViewData[] data = new GraphViewData[numOfPackages];
			
			Log.i("RowTest","The count is " + cData.getCount());
			
			// move to first element in the of the fetched training data
			cData.moveToFirst();
			float force = 0;
			float time = 0;			
			int pacNumber = 0;

			// places data in an array and check if package is missing --> if missing place zeroes in its place.
			for(int i = 0; i< numOfPackages; i++){
				if(pacNumber != cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM))){
					time = pacNumber;
					force = 0;
					data[i] = new GraphViewData(time*MainActivity.SAMPLE_TIME,  force);
				}
				else{
					time = cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM));
					force = cData.getFloat(cData.getColumnIndex(TrainingDbAdapter.KEY_FORCE));
					data[i] = new GraphViewData(time*MainActivity.SAMPLE_TIME,  force);
					cData.moveToNext();
				}
				pacNumber++;
			}
			// adds data to the graph
	    	graph.addSeries(new GraphViewSeries(data));
	    	
	    	// Close the database when done with it.
	    	MainActivity.mDbHelper.close();
	    	
		}catch(Exception e){
			e.printStackTrace();
		}

	}

    /**
     * addjust the settings of the graph
     * @param graphview
     */
	private void graphSettings(GraphView graphview){		
        ((LineGraphView) graphview).setDrawBackground(true);
        ((LineGraphView) graphview).setBackgroundColor(Color.GRAY);
        
        // optional - activate scaling / zooming
        graphview.setScalable(true);
        graphview.setScrollable(true);
        
        // set x-axis
        graphview.setViewPort(0, 10);
        // set y-axis
        graphview.setManualYAxisBounds(1500, 0);
        
        graphview.getGraphViewStyle().setGridColor(Color.WHITE);
        
    }
    
    /**
     * Initialise new Fragment
     * @param text is used to place text on the screen
     * @return
     */
    public static ForceTimeDiagramFrag newInstance(String text, long id) {
    	_id = id;
        ForceTimeDiagramFrag frag = new ForceTimeDiagramFrag();
        Bundle b = new Bundle();
        b.putString("msg", text);

        frag.setArguments(b);

        return frag;
    }
}