package nl.tudelft.util;

public class Training {

	private long _id;
	private String _date;
	private String _rowerName;
	
	/**
	 * init training data
	 * @param id
	 * @param date
	 * @param rowerName
	 */
	public void Trainig(long id, String date, String rowerName){
		this._id = id;
		this._date = date;
		this._rowerName = rowerName;
	}
	
	public void setId(long id){
		this._id = id;
	}
	
	public long getId(){
		return this._id;
	}
	
	public void setDate(String date){
		this._date = date;
	}
	
	public String getDate(){
		return this._date;
	}

	public String getRowerName() {
		return _rowerName;
	}

	public void setRowerName(String _rowerName) {
		this._rowerName = _rowerName;
	}
}
