package nl.tudelft.util;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
 
public class GPSTracker extends Service implements LocationListener {
 
	private final Context mCtx;
	
    // flag for GPS status
    boolean isGPSEnabled = false;
 
    // flag for network status
    boolean isNetworkEnabled = false;
 
    // flag for GPS status
    boolean canGetLocation = false;
 
    Location locationGPS;
    Location locationNetwork;
    Location location;
    
    double latitudeGPS;
    double longitudeGPS; 
    
    double latitudeNetwork;
    double longitudeNetwork; 
    
    double latitude;
    double longitude; 
    
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE = 0;
 
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME = 0;
 
    // Declaring a Location Manager
    protected LocationManager locationManager;
 
    public GPSTracker(Context context) {
    	Log.i("GPS", "GPS initialised!");
        this.mCtx = context;
        getLocation();
    }
    
    /**
     * Fetch the location from GPS and the network provider then compare them and return the best result
     * @return Location 
     */
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mCtx.getSystemService(LOCATION_SERVICE);
 
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            
            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            	Log.w("GPS", "GPS and Network are not enabled!");
            }
            else {
                this.canGetLocation = true;
//              First get location from Network Provider
	             if (isNetworkEnabled) {
	             	// request location update
	                 locationManager.requestLocationUpdates(
	                         LocationManager.NETWORK_PROVIDER,
	                         MIN_TIME,
	                         MIN_DISTANCE, this);
	                 if (locationManager != null) {
	                 	// retrieve last known location and the latitude and longitude.
	                     locationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                     
	                     if (locationNetwork != null) {
	                         latitudeNetwork = locationNetwork.getLatitude();
	                         longitudeNetwork = locationNetwork.getLongitude();
	                     }
	                 }
	             }
                
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME,
                            MIN_DISTANCE, this);
                    if (locationManager != null) {
                        locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        
                        if (locationGPS != null) {
                            latitudeGPS = locationGPS.getLatitude();
                            longitudeGPS = locationGPS.getLongitude();
                        }
                        else{
                        	Log.i("GPS", "Location is null" );
                        }
                    }
                }
                
                if(locationGPS.getAccuracy() < locationNetwork.getAccuracy()){
                	location = locationGPS;
                	latitude = latitudeGPS;
                	longitude = longitudeGPS;
                	Log.d("GPS", "GPS Provider   accuracy Network = " + locationNetwork.getAccuracy() + "; accuracy GPS = " + locationGPS.getAccuracy());
                }
                else{
                	location = locationNetwork;
                	latitude = latitudeNetwork;
                	longitude = longitudeNetwork;
                	Log.d("GPS", "Network Provider and accuracy: " + locationNetwork.getAccuracy() + " accuracy GPS = " + locationGPS.getAccuracy());
                }
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return location;
    }
    
    /**
     * check if the provider is enabled. If true the canGetLocation is set on true.
     */
    public void isProviderEnabled(){
        try {
            locationManager = (LocationManager) mCtx.getSystemService(LOCATION_SERVICE);
 
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
 
            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            	Log.w("GPS", "GPS and Network are not enabled!");
            } 
            else
                this.canGetLocation = true;
            }
        catch(Exception e){}
    }
     
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
        }       
    }
     
    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
         
        // return latitude
        return latitude;
    }

    /**
     * Function to get latitude
     * */
    public double getAccuracy(){
        if(location != null){
            latitude = location.getAccuracy();
        }
        // return latitude
        return latitude;
    }
     
    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        // return longitude
        return longitude;
    }
     
    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }
     
    /**
     * Function to show settings alert dialog. Can be used to send the user to the gps settings menu.
     * */
    public void showGpsSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mCtx);
      
        // Setting Dialog Title
        alertDialog.setTitle("GPS is not enabled");
  
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu? If not enabled the distance can not be measured.");
  
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mCtx.startActivity(intent);
            }
        });
  
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });
  
        // Showing Alert Message
        alertDialog.show();
    }
 
    @Override
    public void onLocationChanged(Location location) {
    	
    }
 
    @Override
    public void onProviderDisabled(String provider) {
    }
 
    @Override
    public void onProviderEnabled(String provider) {
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
 
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    
    /** 
     *  return distance between old and new location
     * @param newLocation
     * @param oldLocation
     * @return distance (in meters)
     */
    public float getDistance(Location newLocation, Location oldLocation){
    	return oldLocation.distanceTo(newLocation);
    }
}