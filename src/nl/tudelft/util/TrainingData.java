package nl.tudelft.util;

public class TrainingData {
	
	// private variables
	private long _id;
	private long _sampleNum;
	private float _force;
	private float _angle;
	private double _longitude;
	private double _latitude;
	private long _heartRate;
	
	/** 
	 * init training data
	 * @param id
	 * @param sampleNum
	 * @param force
	 * @param angle
	 * @param longitude
	 * @param latitude
	 */
	public TrainingData(long id, long sampleNum, float force, float angle, double longitude, double latitude){
		this._id = id;
		this._sampleNum = sampleNum;
		this._force = force;
		this._angle = angle;
		this._longitude = longitude;
		this._latitude = latitude;
		this._heartRate = 0;
	}
	
	public TrainingData(long id, long sampleNum, float force, float angle, double longitude, double latitude, long heartRate){
		this._id = id;
		this._sampleNum = sampleNum;
		this._force = force;
		this._angle = angle;
		this._longitude = longitude;
		this._latitude = latitude;
		this._heartRate = heartRate;
	}
	
	public void setId(long id){
		this._id = id;
	}
	
	public long getId(){
		return this._id;
	}
	
	public void setPackageNum(long sampleNum){
		this._sampleNum = sampleNum;
	}
	
	public long getPackageNum(){
		return this._sampleNum;
	}
	
	public void setForce(float force){
		this._force = force;
	}
	
	public float getForce(){
		return this._force;
	}
	
	public void setAngle(float angle){
		this._angle = angle;
	}
	
	public float getAngle(){
		return this._angle;
	}
	
	public void setLongitude(double longitude){
		this._longitude = longitude;
	}
	
	public double getLongitude(){
		return this._longitude;
	}
	
	public void setLatitude(double latitude){
		this._latitude = latitude;
	}
	
	public double getLatitude(){
		return this._latitude;
	}
	
	public void setHeartRate(long heartRate){
		this._heartRate = heartRate;
	}
	
	public long getHeartRate(){
		return this._heartRate;
	}
	
}
