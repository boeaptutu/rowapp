package nl.tudelft.util;

import android.content.Context;
import android.location.Location;

public class GPS {
	private GPSTracker gpsTracker;
	private Context mCtx;
	private Location oldLocation = null;
	private Location newLocation = null;
	private float distance = 0;
	private float deltaDistance = 0;
	private final int ACCURACY_THRESHOLD = 50;
	
	
	/**
	 * Constructor to initialize GPS
	 * @param context
	 */
	public GPS(Context context){
		this.mCtx = context;
		gpsTracker = new GPSTracker(mCtx);
	}
	
	/**
	 * Updates the distance, and the new and previous locations.
	 * @return integer if -1 then no new location is available yet.
	 */
	public boolean updateLocation(){
		Location location = gpsTracker.getLocation();
		if (location != newLocation){
			// if the accuracy of the location is smaller than the accuracy threshold do not update the distance.
			if(ACCURACY_THRESHOLD > location.getAccuracy()){
				oldLocation = newLocation;
				newLocation = location;
				
				if(oldLocation != null)
					distance = calcDistance();
				
				return true;
			}else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	/**
	 * calculate the total distance. Every new location will be used to calculate 
	 * a new delta distance if this is  bigger than 3 meters then it will be used to calulate the new total distance.
	 * @return total distance
	 */
	private float calcDistance(){
		if(oldLocation != null){
			deltaDistance =  oldLocation.distanceTo(newLocation);
			if(deltaDistance > 3.0){
				distance += deltaDistance;
				oldLocation = newLocation;
				return distance;
			}
			return distance;
		}
		return 0;
	}
	
	/**
	 *  Check if one provider is enabled.
	 */
	public void isProviderEnabled(){
		gpsTracker.isProviderEnabled();
	}
	
	/**
	 * total distance in meters
	 * @return distance  (float)
	 */
	public float getDistance(){
		return distance; 
	}
	
	/**
	 *  return latitude of the location 
	 * @return latitude  (double)
	 */
	public double getLatitude(){
		return newLocation.getLatitude();
	}
	
	/**
	 *  return longitude of the location
	 * @return longitude  (double)
	 */
	public double getLongitude(){
		return newLocation.getLongitude();
	}
	
	/**
	 *  return accuracy of the location
	 * @return Accuracy (double)
	 */
	public double getAccuracy(){
		return newLocation.getAccuracy();
	}
	
	/**
	 *  return longitude of the location
	 * @return longitude
	 */
	public boolean canGetLocation(){
		return gpsTracker.canGetLocation;
	}
	
	public void showSettingsAlert(){
		if(!gpsTracker.isGPSEnabled)
			gpsTracker.showGpsSettingsAlert();
	}
	
	/**
	 * return a boolean if the network is enabled or not True if enabled, false if not
	 * @return isNetworkEnabled (boolean)
	 */
	public boolean getNetworkEnabled(){
		return gpsTracker.isNetworkEnabled;
	}
	
	/**
	 *  Stop gpsTracker
	 */
	public void close(){
		gpsTracker.stopUsingGPS();
	}
	
	/**
	 * retreive the last saved location 
	 * @return location (Location)
	 */
	public Location getLocation(){
		return gpsTracker.getLocation();
	}
}
