package nl.tudelft.util;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class TrainingDbAdapter {
	
	// Common column name
	public static final String KEY_ID = "_id";
	
	// Training Column names
	public static final String KEY_DATE = "date";
	public static final String KEY_DISTANCE = "distance";
	public static final String KEY_TIME = "time";
	public static final String KEY_ROWERNAME = "rowerName";
	
	//TrainingData Column names
	public static final String KEY_SAMPLENUM = "sampleNum";
	public static final String KEY_FORCE = "force";
	public static final String KEY_ANGLE = "angle";
	public static final String KEY_LONGITUDE = "longitude";
	public static final String KEY_LATITUDE = "latitude";
	public static final String KEY_HEARTRATE = "heartRate";
	public static final String KEY_REALTIME = "realtime";
	
	
	private static final String TAG = "TrainingDbAdapter";
	private DbHelper mDbHelper;
	private SQLiteDatabase mDb;
	
	// Table Names
	private static final String TABLE_TRAINING = "training";
	private static final String TABLE_TRAININGDATA = "trainingData";
	
	/**
	 * SQL statement for creating the Table Training
	 */
	private static final String SQL_CREATE_TABLE_TRAININGDATA = "CREATE TABLE " + TABLE_TRAININGDATA + 
			"("
			+ KEY_ID 			+ " INTEGER,"
			+ KEY_SAMPLENUM 	+ " INTEGER,"
			+ KEY_FORCE 		+ " REAL," 
			+ KEY_ANGLE 		+ " REAL,"
			+ KEY_LONGITUDE		+ " REAL," 
			+ KEY_LATITUDE		+ " REAL,"
			+ KEY_HEARTRATE		+ " INTEGER,"
			+ KEY_REALTIME		+ " TEXT"
			+ ")";
	
	/**
	 *  SQL Statement for creating the TrainingData Table		
	 */
	private static final String SQL_CREATE_TABLE_TRAINING = "CREATE TABLE " + TABLE_TRAINING + 
			"("
			+ KEY_ID 		+ " INTEGER PRIMARY KEY,"
			+ KEY_DATE 		+ " INTEGER," 
			+ KEY_ROWERNAME + " TEXT"
			+ ")";
	
	/** 
	 * Query for creating and destroying database
	 */
	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ";
	
	// Database Version and Name
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "RowData.db";
	
	private Context mCtx;
	
	/**
	 * @author kajdreef
	 * Create DbHelper
	 */
	public class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context){
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		/**
		 * When database created create entities for database
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			// create Training and TrainingData table
			db.execSQL(SQL_CREATE_TABLE_TRAINING);
			db.execSQL(SQL_CREATE_TABLE_TRAININGDATA);
		}
	
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
			// Delete the tables if they exists 
            db.execSQL(SQL_DELETE_ENTRIES + TABLE_TRAININGDATA);
			db.execSQL(SQL_DELETE_ENTRIES + TABLE_TRAINING);
			// Create new tables
	        onCreate(db);
		}
	}

	/**
	 *  create a new training
	 * @param id
	 * @param date
	 * @param rowerName
	 * @return
	 */
	public long createTraining(long id, String date, String rowerName){
		
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_ID, id);
        initialValues.put(KEY_DATE, date);
        initialValues.put(KEY_ROWERNAME, rowerName);
        
        Log.i("SQLwrite", "training created" );
        return mDb.insert(TABLE_TRAINING, null, initialValues);
	}
	
	/**
	 * Create new trainingdata row in database
	 * @param id
	 * @param sampleNum
	 * @param force
	 * @param angle
	 * @param longitude
	 * @param latitude
	 * @param heartRate
	 * @param realtime
	 * @return
	 */
	public long createTrainingData(long id, long sampleNum, float force, float angle, double longitude, double latitude, long heartRate, String realtime){
		
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_ID, id);
		initialValues.put(KEY_SAMPLENUM, sampleNum);
		initialValues.put(KEY_FORCE, force);
		initialValues.put(KEY_ANGLE, angle);
		initialValues.put(KEY_LONGITUDE, longitude);
		initialValues.put(KEY_LATITUDE, latitude);
		initialValues.put(KEY_HEARTRATE, heartRate);
		initialValues.put(KEY_REALTIME, realtime);
		
		Log.i("SQLwrite", "training data created: " + sampleNum );
		return mDb.insert(TABLE_TRAININGDATA, null, initialValues);
	}
	
	
    public TrainingDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }
	
    public TrainingDbAdapter open() throws SQLException {
        mDbHelper = new DbHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

	public void close(){
		mDbHelper.close();
	}
	
	/**
	 * @param trainingId
	 * @return all the training data of a certain trainingId
	 */
	public Cursor fetchTrainingData(long trainingId) {
		
		Cursor mCursor = mDb.query(true,TABLE_TRAININGDATA , new String[] {KEY_ID, KEY_FORCE, KEY_ANGLE, KEY_SAMPLENUM, KEY_LONGITUDE, KEY_LATITUDE, KEY_HEARTRATE, KEY_REALTIME}
						, KEY_ID + "=" + trainingId, null, null, null, KEY_SAMPLENUM + " ASC", null, null);
		if(mCursor != null)
			mCursor.moveToFirst();
		
		return mCursor;	
	}
	
	/**
	 * @param trainingId
	 * @param packetNum
	 * @return all training data with sample number higher than packetNum and with a specified trainingId
	 */
	public Cursor fetchTrainingData(long trainingId, long packetNum){
		Cursor mCursor = mDb.query(true,TABLE_TRAININGDATA , new String[] {KEY_ID, KEY_FORCE, KEY_ANGLE, KEY_SAMPLENUM, KEY_LONGITUDE, KEY_LATITUDE, KEY_HEARTRATE, KEY_REALTIME}
						, KEY_SAMPLENUM + ">=" + packetNum + " AND " + KEY_ID + "=" + trainingId , null, null, null,  KEY_SAMPLENUM + " ASC", null, null);
		if(mCursor != null)
			mCursor.moveToFirst();
		
		return mCursor;
	}
		
	/**
	 * return all trainings in the database
	 * @return
	 */
	public Cursor fetchAllTrainings(){
		return mDb.query(TABLE_TRAINING, new String[] {KEY_ID, KEY_DATE, KEY_ROWERNAME}, null, null, null, null, KEY_ID + " ASC");
	}
	
	/**
	 * return all Trainings with a certain rowerName
	 * @param rowerName
	 * @return list of all the trainings a certain rower did
	 */
	public Cursor fetchAllTrainingRowerName(String rowerName){
		Cursor mCursor =  mDb.query(true, TABLE_TRAINING, new String[] {KEY_ID, KEY_DATE, KEY_ROWERNAME}
						, KEY_ROWERNAME + "=" + rowerName, null, null, null, KEY_ID + " ASC", null, null);
		
		if(mCursor != null)
			mCursor.moveToFirst();
		
		return mCursor;
	}

	/**
	 * @return check what the last trainingId was and return that. It there is no training yet in the database return -1
	 */
	public int fetchLastTrainingId(){
		Cursor mTrainingId = mDb.rawQuery("SELECT MAX( "+ KEY_ID +" )" + " FROM " + TABLE_TRAINING, null);
		
		if(mTrainingId.moveToFirst()){
			int iData = mTrainingId.getInt(0);
			mTrainingId.close();
			return iData;
		}
		else{
			Log.i("RowTest", "Nothing was found!");
			mTrainingId.close();
			return -1;
		}
	}
	
	/**
	 * delete the training and data of a certain trainingId
	 * @param id
	 */
	public void deleteTraining(long id){
		try{
			mDb.delete(TABLE_TRAINING, KEY_ID + "=" + id, null);
			mDb.delete(TABLE_TRAININGDATA, KEY_ID + "=" + id, null);
		} catch(Exception e){}
	}
	
	/**
	 * Delete complete database
	 */
	public void deleteDatabaseEntries(){
		try{
			mDb.delete(TABLE_TRAINING, null, null);
			mDb.delete(TABLE_TRAININGDATA, null, null);
		} catch(Exception e){}
	}
}