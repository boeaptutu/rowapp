/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.tudelft.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import nl.tudelft.rowapp.MainActivity;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    
    
    private int packageNum = 0;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
	
	public Location location;
    
    public final static String ACTION_GATT_CONNECTED =
            "bluetooth.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "bluetooth.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "bluetooth.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "bluetooth.ACTION_DATA_AVAILABLE";
    public final static String ACTION_DATA_HEART_RATE_AVAILABLE =
            "bluetooth.ACTION_DATA_HEART_RATE_AVAILABLE";
    public final static String ACTION_DATA_OAR_AVAILABLE =
            "bluetooth.ACTION_DATA_OAR_AVAILABLE";
    public final static String EXTRA_DATA_HEART_RATE =
            "bluetooth.EXTRA_DATA_HEART_RATE";
    public final static String EXTRA_DATA_OAR =
            "bluetooth.EXTRA_DATA_OAR";
    public final static String EXTRA_DATA =
            "bluetooth.EXTRA_DATA";
    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString(GattAttributes.HEART_RATE_MEASUREMENT);
    public final static UUID UUID_OAR_DATA =
            UUID.fromString(GattAttributes.OAR_DATA);
    
    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        
        /**
         * callback when new services are detected.
         */
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        /**
         * callback when result from read operation is received.
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                extractData(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        /**
         * callback when notification is received
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {

        	if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
        		extractData(ACTION_DATA_HEART_RATE_AVAILABLE, characteristic);
        	}
        	else if (UUID_OAR_DATA.equals(characteristic.getUuid())) {
        		extractData(ACTION_DATA_OAR_AVAILABLE, characteristic);
        	}
        }
    };

    /**
     * broadcasts String 'action' to listening classes.
     * Will be used for connection status and detected services update.
     * @param action
     */
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    /**
     * Will extract a received data packet. The extracted data will be stored in the database.
     * @param action
     * @param characteristic
     */
    private void extractData(final String action,
                                 final BluetoothGattCharacteristic characteristic) {

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d(TAG, "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d(TAG, "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
        } else 
        	
        	
        // Own created profile for extracting data from the oar.
        if (UUID_OAR_DATA.equals(characteristic.getUuid())) {
        	final byte[] data = characteristic.getValue();
        	
        	if (data != null && data.length == 20) {   

        	double longitude = location.getLongitude();
        	double latitude = location.getLatitude();
        	
        	int identifier = (((data[16] & 0xFF) & 63)<<24) | ((data[17] & 0xFF) <<16) | ((data[18]& 0xFF) <<8) | (data[19] & 0xFF);
        	int angle = ((data[0] & 0xFF) << 6) | ((data[1] & 0xFF) >> 2);
        	float anglef = (float)(2*Math.PI)*((float)angle/16384f);
        	int force = ((((data[1] & 0xFF) & 3) << 10 ) | ((data[2] & 0xFF) << 2))  | ((data[3] & 0xFF) >>6);
        	int sampleNumber = (identifier) * 5;
        	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , sampleNumber, force, anglef, longitude, latitude, 80, " ");
        	Log.d("SENSOR_VALUE", String.format("1: "+ "Angle: %d  Force: %d", angle, force) );		
        	
        	angle = (((data[3] & 0xFF) & 63) << 8) | (data[4] & 0xFF);
        	anglef = (float)(2*Math.PI)*((float)angle/16384f);
        	force = ((data[5] & 0xFF)<<4) | ((data[6]& 0xFF)>>4);
        	sampleNumber = (identifier) * 5 + 1;
        	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , sampleNumber, force, anglef, longitude, latitude, 80, " ");
        	Log.d("SENSOR_VALUE", String.format("2: "+"Angle: %d  Force: %d", angle, force) );
        	
        	angle = (((data[6] & 0xFF) & 15) << 10) | ((data[7] & 0xFF)<<2) | ((data[8] & 0xFF) >> 6);
        	anglef = (float)(2*Math.PI)*((float)angle/16384f);
        	force = (((data[8] & 0xFF) & 63) << 6) | ((data[9] & 0xFF)>>2);
        	sampleNumber = (identifier) * 5 + 2;
        	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , sampleNumber, force, anglef, longitude, latitude, 80, " ");
        	Log.d("SENSOR_VALUE", String.format("3: "+"Angle: %d  Force: %d", angle, force) );
        	
        	angle = (((data[9] & 0xFF) & 3) << 12) | ((data[10] & 0xFF)<<4) | ((data[11] & 0xFF) >> 4);
        	anglef = (float)(2*Math.PI)*((float)angle/16384f);
        	force = (((data[11] & 0xFF) & 15) << 8) | (data[12] & 0xFF);
        	sampleNumber = (identifier) * 5 + 3;
        	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , sampleNumber, force, anglef, longitude, latitude, 80, " ");
        	Log.d("SENSOR_VALUE", String.format("4: "+"Angle: %d  Force: %d", angle, force) );
        	
        	angle = ((data[13] & 0xFF) << 6) | ((data[14] & 0xFF) >> 2);
        	anglef = (float)(2*Math.PI)*((float)angle/16384f);
        	force = ((((data[14] & 0xFF) & 3) << 10 ) | ((data[15] & 0xFF) << 2))  | ((data[16]& 0xFF) >>6);
        	sampleNumber = (identifier) * 5 + 4;
        	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , sampleNumber, force, anglef, longitude, latitude, 80, " ");
        	Log.d("SENSOR_VALUE", String.format("5: "+"Angle: %d  Force: %d", angle, force) );
        	
        	
        	Log.d("SENSOR_VALUE", String.format("identifier: %d", identifier) );
        	
        	
        	
        	
        }     	

        	
/**
 * Old code to test the communication. Code will be replaced with commented code above a.s.a.p.
 */	
//            if (data != null && data.length > 0) { //2 bytes for the force and 2 bytes for the angle
//            	Log.d(TAG,String.format("aantal bytes ontvangen: %d", data.length));
//            	
//            	//for(int i=0; i<20; i+=4){
//            	int i=0;
//            	short angle = (short)(((data[i] & 0xFF) << 8) | (data[i+1] & 0xFF));
//            	short force = (short)(((data[i+2] & 0xFF) << 8) | (data[i+3] & 0xFF));
//            	Log.d("SENSOR_VALUE", String.format("Angle: %d  Force: %d", angle, force) );
//            	
//            	String realtime = " ";
//            	MainActivity.mDbHelper.createTrainingData(MainActivity.mDbHelper.fetchLastTrainingId() , packageNum, force, angle, location.getLongitude(), location.getLatitude(), 80, realtime);
//            	packageNum++;
//
//            }
/**
 * end of old code to test communication.
 */	  
            
        } 
        
    }

    public class LocalBinder extends Binder {
        BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        // This is specific to Heart Rate Measurement.
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
        
        // This is specific to oar Measurement.
        if (UUID_OAR_DATA.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }
}
