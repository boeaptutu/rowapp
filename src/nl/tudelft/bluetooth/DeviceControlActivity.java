/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.tudelft.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import nl.tudelft.dataProcessing.ProcessData;
import nl.tudelft.rowapp.ImmersiveMode;
import nl.tudelft.rowapp.MainActivity;
import nl.tudelft.util.TrainingDbAdapter;
import nl.tudelft.bluetooth.DeviceScanActivity;

import com.example.changingactivities.R;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static ProcessData processData;
    private int lastSample = 0;
    private int startSample = 0;
    private int previousSample = 0;
    private float previousForce = 0;
	private float previousAngle = 0;
    
    private Thread time;
    private Thread dataThread;
	private Thread gpsThread;
	private long refreshTime = 8000; // refresh every 8 seconds
	TextView textview;
	
  
    private TextView mDataFieldDistance;
    private TextView mDataFieldForce;
    private TextView mDataFieldPower;
    private TextView mDataFieldTime;
    private TextView mDataFieldSPM;
    
    
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    

    /**
     * Code to manage Service lifecycle.
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                invalidateOptionsMenu();
                Toast.makeText(getApplicationContext(), "Connection established", Toast.LENGTH_SHORT).show();   
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                invalidateOptionsMenu();
                clearUI();
                Toast.makeText(getApplicationContext(), "Connection lost", Toast.LENGTH_SHORT).show();    
        		reconnecting();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
            	notifyGattServices(mBluetoothLeService.getSupportedGattServices());
            } 
        }
    };


    
    /**
     * will enable the notify flag for the given characteristic if possible. otherwise if possible it will read the value of the 
     * characteristic
     * @param characteristice
     * @return true if characteristic has property to notify otherwise false
     */
    private boolean enableNotify(BluetoothGattCharacteristic characteristic){
    	 final int charaProp = characteristic.getProperties();
//         if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
//             mBluetoothLeService.readCharacteristic(characteristic);
//         }
         if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
             mNotifyCharacteristic = characteristic;
             mBluetoothLeService.setCharacteristicNotification(
                     characteristic, true);
             return true;
         }
         return false;
    }

    /**
     * will clear the UI if device is disconnected.
     */
    private void clearUI() {
    //    mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);	
    	mDataFieldDistance.setText(R.string.no_data);
        mDataFieldSPM.setText(R.string.no_data);   
        mDataFieldForce.setText(R.string.no_data);
        mDataFieldPower.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.gatt_services_characteristics);
        setContentView(R.layout.activity_real_time_data);
        ImmersiveMode.fullScreen(getWindow());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        processData = new ProcessData();
        
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        //((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mDataFieldDistance = (TextView) findViewById(R.id.Real_Time_Distance);
        mDataFieldForce = (TextView) findViewById(R.id.Real_Time_Force);
        mDataFieldPower = (TextView) findViewById(R.id.Real_Time_Power);
        mDataFieldTime = (TextView) findViewById(R.id.Real_Time_Past);
        mDataFieldSPM = (TextView) findViewById(R.id.Real_Time_SPM);
        MainActivity.mDbHelper.open();
        //start timer so rowing time could be calculated.
        processData.startTimer();
         
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        //every second the time will be updated.
	    time = new Thread() {
	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(1000);
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        	updateTime();
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };
    	
	    //every 'refreshTime' the GPS location will be updated
	    gpsThread = new Thread() {
	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(refreshTime);
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        		doGpsStuff();
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };
	    

	    //every 10 seconds the rowing data will be refreshed and displayed at the screen.
	    dataThread = new Thread() {
	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(10000);
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        	updateDisplay();
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };
	    
	    dataThread.start();
	    gpsThread.start();
	    time.start(); 
    }
    
    /**
     * updates location --> new location and calculates the new distance
     */
    public void doGpsStuff(){
		if(DeviceScanActivity.gps.updateLocation()){
	            double distance = DeviceScanActivity.gps.getDistance();
	            if(mConnected){
	            	mDataFieldDistance.setText(String.valueOf(distance));
	            }
	            mBluetoothLeService.location = DeviceScanActivity.gps.getLocation();
		}   
    }

    @Override
    protected void onResume() {
        super.onResume();
        ImmersiveMode.fullScreen(getWindow());
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        time.interrupt();
        time = null;
        
        dataThread.interrupt();
        dataThread = null;
        
        destroyGps();
        MainActivity.mDbHelper.close();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
        super.onDestroy();
    }
    
    private void destroyGps(){
        gpsThread.interrupt();
        gpsThread = null;
    }

    private void reconnecting(){
		Toast.makeText(getApplicationContext(), "Trying to reconnect", Toast.LENGTH_LONG).show(); 
        if (mBluetoothLeService != null) {
           mBluetoothLeService.connect(mDeviceAddress);
        }
    }
    
    
    /**
     * function will process all training data since last time the function is entered. If function is entered for the first time,
     * the starting sample will start at zero.
     * If no samples are available in the database the function will do nothing.
     */
    private void updateDisplay() {
    	
    	Log.d("screenRefresh", "screen Refresh");
    	
		// fetch all new data from database since last time
		Cursor cData = MainActivity.mDbHelper.fetchTrainingData(MainActivity.mDbHelper.fetchLastTrainingId(),startSample);

		//exit when no data is in the database.
		if(!cData.moveToFirst()){
			Log.e("PACKETLOSS", "no samples found");
			return;
		}
    	
		previousSample = cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM));
		cData.moveToLast();
		lastSample = cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM));
		cData.moveToFirst();
		while(cData.moveToNext()){
			
			
			startSample = cData.getInt(cData.getColumnIndex(TrainingDbAdapter.KEY_SAMPLENUM));

			 //check if packets before 'last packet' are received.
			//the packets before could still be received.
			if((startSample == (lastSample-4)) && (startSample - previousSample != 1) ){
				startSample = previousSample;
				break;
			}
			
			
			//normal process for consecutive packets 
			if(startSample - previousSample == 1){
				float force = cData.getFloat(cData.getColumnIndex(TrainingDbAdapter.KEY_FORCE));
				float angle = cData.getFloat(cData.getColumnIndex(TrainingDbAdapter.KEY_ANGLE));
				Log.d("dataThread", startSample + ":  angle: "+angle+ "   force: "+force);
				previousForce = force;
				previousAngle = angle;
				processData.calculateData(angle, force);
			} 
			
			// process for a lost packet
			else{
				int sampleslost = startSample - previousSample;
				float force = cData.getFloat(cData.getColumnIndex(TrainingDbAdapter.KEY_FORCE));
				float angle = cData.getFloat(cData.getColumnIndex(TrainingDbAdapter.KEY_ANGLE));
				float missingForceInterval = (force - previousForce) / sampleslost;
				float missingAngleInterval = (angle - previousAngle) / sampleslost;
				
				for(int i = 1; i<sampleslost; i++){
					float tempAngle = previousAngle + missingAngleInterval * i;
					float tempForce = previousForce + missingForceInterval * i;
					Log.d("PACKETLOSS", "estimated angle: "+tempAngle + "    force: "+ tempForce);
					processData.calculateData(tempAngle, tempForce);			
				}
				previousForce = force;
				previousAngle = angle;
				processData.calculateData(angle, force);	
				
				
			}
			previousSample = startSample;
		}
		
		
    	
    	/**
    	 * Debug end
    	 */
    	
    	
    	
		//display's the power, SPM and force to the display if device is connected.
		if(mConnected){
			float powerFloat = processData.displayPower();
			String powerString = String.valueOf(powerFloat);
			mDataFieldPower.setText(powerString);
			
			float spmFloat = processData.displaySPM();
			String spmString = String.valueOf(spmFloat);
			mDataFieldSPM.setText(spmString);
			
			float forceFloat = processData.displayForce();
			String forceString = String.valueOf(forceFloat);
			mDataFieldForce.setText(forceString);
		}
    }
    
    /**
     * display's current rowing time to the display.
     * this function will be called from the time thread.
     */
    public void updateTime() {
		String timePast = processData.DisplayTotalTime();
		mDataFieldTime.setText(timePast);
    }

    /**
     * check if discovered service's contains the Rowing panel service and enable notify's if it does. 
     * @param gattServices
     */
    private void notifyGattServices(List<BluetoothGattService> gattServices) {
    	if (gattServices == null) return;
    	String uuid = null;
    	String Rowing_Panel_Service = GattAttributes.ROWING_PANEL_SERVICE;
    	
    	for (BluetoothGattService gattService : gattServices) {
    		uuid = gattService.getUuid().toString();
    		if(uuid.equals(Rowing_Panel_Service)){
    			 List<BluetoothGattCharacteristic> gattCharacteristics =
    	                    gattService.getCharacteristics();
    			 for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
    	                uuid = gattCharacteristic.getUuid().toString();
    	                if(uuid.equals(GattAttributes.OAR_DATA)){

    	                	DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    	                	Date today = Calendar.getInstance().getTime();
    	                	String trainingDate = format.format(today);
    	                	
    	                	String nameRower = "default";
    	                	MainActivity.mDbHelper.createTraining(MainActivity.mDbHelper.fetchLastTrainingId()+1, trainingDate, nameRower);

    	                	enableNotify(gattCharacteristic);
    	                	/* this should be enabled if multiple characteristic should notify at start of the program.
    	                	 * because of the delay the BLE code has more time to process the notify request.
    	                	try {
    	                	    Thread.sleep(500);
    	                	} catch(InterruptedException ex) {
    	                	    Thread.currentThread().interrupt();
    	                	}
    	                	*/
    	                }
    			 }
    			 
    		}
    		
    	}
    	
    	
    }

    /**
     * Filters to receive broadcast event.
     * @return
     */
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_HEART_RATE_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_OAR_AVAILABLE);       
        return intentFilter;
    }
}
