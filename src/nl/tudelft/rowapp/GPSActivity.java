package nl.tudelft.rowapp;
 
import com.example.changingactivities.R;

import nl.tudelft.util.GPS;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
 
public class GPSActivity extends Activity {
    
	private Thread gpsThread;
	private long refreshTime = 1000; // refresh every 5 seconds
	private boolean threadOn = false;
	
    Button showLocation;
    TextView textview;
    
    // GPSTracker class
    GPS gps;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_gps);
        ImmersiveMode.fullScreen(getWindow());
        
        // create class object
        gps = new GPS(GPSActivity.this);
        
        if(!gps.canGetLocation()){
        	gps.showSettingsAlert();
        }
        
        gpsThread = new Thread() {
	        @Override
	        public void run() {
	            try {
	                while (!isInterrupted()) {
	                    Thread.sleep(refreshTime);
	                    runOnUiThread(new Runnable() {
	                        @Override
	                        public void run() {
	                        	if(threadOn){
	                        		doGpsStuff();
	                        	}
	                        }
	                    });
	                }
	            } catch (InterruptedException e) {
	            }
	        }
	    };
	    
	    gpsThread.start();
        
        
    }
    
    
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		ImmersiveMode.fullScreen(getWindow());
	}
    
    @Override
    public void onResume(){
    	super.onResume();
    	ImmersiveMode.fullScreen(getWindow());
    	
    	//check if provider is enabled
        gps.isProviderEnabled();
        showLocation = (Button) findViewById(R.id.showLocation);
        
        // show location button click event
        showLocation.setOnClickListener(new View.OnClickListener() {
             
            @Override
            public void onClick(View arg0) {
                // check if GPS enabled  
            	if(threadOn){ 
            		threadOn = false;
            	}
            	else{
            		threadOn = true;
            	}
            	
            }
        });
    }
    
    /**
     * updates the location, process the data and put in on the screen.
     */
    public void doGpsStuff(){
		gps.getLocation();
    	// updates location --> new location and calculates the new distance
    	if(gps.updateLocation()){
                Location location = gps.getLocation();
                
                // display info on screen
                textview = (TextView) findViewById(R.id.GPS_view1);
                textview.setText(String.valueOf(location.getLatitude()));
                
                textview = (TextView) findViewById(R.id.GPS_view2);
                textview.setText(String.valueOf(location.getLongitude()));
                
                textview = (TextView) findViewById(R.id.GPS_view3);
                textview.setText(String.valueOf(location.getAccuracy()));
                
                textview = (TextView) findViewById(R.id.GPS_view4);
                textview.setText(String.valueOf(gps.getDistance()));
    	}
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    	gps.close();
    }
    
    @Override
    protected void onDestroy() {
    	gpsThread.interrupt();
    	gpsThread = null;
    	gps.close();
    	gps = null;
    }
     
}