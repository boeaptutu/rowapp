package nl.tudelft.rowapp;

import nl.tudelft.analysisFragments.FirstFragment;
import nl.tudelft.analysisFragments.OverviewFrag;
import nl.tudelft.analysisFragments.ForceTimeDiagramFrag;
import nl.tudelft.util.TrainingDbAdapter;

import com.example.changingactivities.*;

import android.os.Bundle;  
import android.support.v4.app.Fragment;  
import android.support.v4.app.FragmentActivity;  
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;  
import android.support.v4.view.ViewPager;  
import android.util.Log;
import android.view.View;

// use FragmentActivity for the SwipeView
public class AnalysisActivity extends FragmentActivity {
	private long id = 0;
	
	// set up viewPager
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          
        // get the info you passed through from AnalysisTrainingChoiceActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null){
        	id = extras.getLong(TrainingDbAdapter.KEY_ID);
        }
        setContentView(R.layout.activity_analysis);
        ImmersiveMode.fullScreen(getWindow());
        
        // set ViewPager
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));
    }
    
    
	@Override
	public void onResume() {
		super.onResume();
		ImmersiveMode.fullScreen(getWindow());
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		ImmersiveMode.fullScreen(getWindow());
	}
    
    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    	
        final int PAGE_COUNT = 3;
        
        /**
         * constructor and uses the fragment manager to use the multiple fragments
         * @param fm
         */
        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        /**
         *  gets the amount of Pages(non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
        
        /**
         *  Creates the page.(non-Javadoc)
         * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
         */
        @Override
        public Fragment getItem(int position) {
        	switch(position) {
	            case 0: return OverviewFrag.newInstance("OverviewFrag", id);
	            case 1: return FirstFragment.newInstance("FirstFragment", id);
	            case 2: return ForceTimeDiagramFrag.newInstance("ForceTimeDiagramFrag", id);
	            default:  return FirstFragment.newInstance("FirstFragment, Default", id);
	            }
        }
        
        /**
         * Add the page title names per position. 
         */
        public CharSequence getPageTitle(int position) {
        	if (position == 0){
        		return "Overview";
        	}
        	else if(position == 1){
        		return "Time Split";
        	}
        	else
        		return "Force-Time Graph";
        }
    }

}