package nl.tudelft.rowapp;

import nl.tudelft.util.TrainingDbAdapter;
import nl.tudelft.rowapp.AnalysisActivity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.changingactivities.R;

public class AnalysisTrainingChoiceActivity extends ListActivity {

	Cursor mTraining;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	// set Layout to the activity analysis list
    	setContentView(R.layout.activity_analysis_list);
    	ImmersiveMode.fullScreen(getWindow());
    	
    	fillData();
    }
    
    public AnalysisTrainingChoiceActivity getContext(){
    	return this;
    }
    
    
	@Override
	public void onResume() {
		super.onResume();
		ImmersiveMode.fullScreen(getWindow());
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		ImmersiveMode.fullScreen(getWindow());
	}
	
	/**
	 * if clicked on item then start new activity and send the key_id with it.
	 */
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Cursor c = mTraining;
        c.moveToPosition(position);
        Intent i = new Intent(this, AnalysisActivity.class);
        i.putExtra(TrainingDbAdapter.KEY_ID, id);
        startActivity(i);
    }

    /**
     * place data on the screen in a list.
     */
	private void fillData() {
    	MainActivity.mDbHelper.open();
        // Get all of the notes from the database and create the item list
        mTraining = MainActivity.mDbHelper.fetchAllTrainings();
        startManagingCursor(mTraining);

        String[] fields = new String[] { TrainingDbAdapter.KEY_ID, TrainingDbAdapter.KEY_DATE };
        int[] view = new int[] { R.id.text1, R.id.text2};
        
        // Now create an array adapter and set it to display using our row
        SimpleCursorAdapter training  =
            new SimpleCursorAdapter(this, R.layout.training_row, mTraining, fields, view);
        setListAdapter(training);		
	}
}
