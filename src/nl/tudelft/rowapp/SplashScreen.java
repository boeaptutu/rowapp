package nl.tudelft.rowapp;

import com.example.changingactivities.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
 
public class SplashScreen extends Activity {
 
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // set layout
        setContentView(R.layout.activity_splash);
        ImmersiveMode.fullScreen(getWindow());
        
        // Show splash screen for a few seconds (depends on SPLASH_TIME_OUT)
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
 
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
    


@Override
public void onResume() {
	super.onResume();
	ImmersiveMode.fullScreen(getWindow());
}

@Override
public void onWindowFocusChanged(boolean hasFocus) {
	super.onWindowFocusChanged(hasFocus);
	ImmersiveMode.fullScreen(getWindow());
}
 
}