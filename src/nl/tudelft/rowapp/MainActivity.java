package nl.tudelft.rowapp;

import nl.tudelft.util.TrainingDbAdapter;
import nl.tudelft.bluetooth.DeviceScanActivity;
import com.example.changingactivities.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
	
	public static TrainingDbAdapter mDbHelper;
	
	public static final float SAMPLE_TIME = (float)(1/40.0); //in Sec
	public static final float SAMPLE_TIME_mS = SAMPLE_TIME*1000; //in mSec

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImmersiveMode.fullScreen(getWindow());
    	mDbHelper = new TrainingDbAdapter(this);
    	
    }  
    
	@Override
	public void onResume() {
		super.onResume();
		ImmersiveMode.fullScreen(getWindow());
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		ImmersiveMode.fullScreen(getWindow());
	}
    
	/**
	 * open DeviceScanActivity
	 * @param view
	 */
    public void initRowing(View view){
	    // create new intent!
	    Intent i = new Intent(this, DeviceScanActivity.class);
	    startActivity(i); 
    }

    /**
     * Open AnalysisTrainingChoiceActivity
     * @param view
     */
    public void initAnalysisChoice(View view){
    	Intent i = new Intent(this, AnalysisTrainingChoiceActivity.class);
    	startActivity(i);	
    }
    
    /**
     * Open GPSActivity used for testing the GPS.
     * @param view
     */
    public void initGPS(View view){
    	Intent i = new Intent(this, GPSActivity.class);
    	startActivity(i);	
    }
}