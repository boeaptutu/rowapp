package nl.tudelft.dataProcessing;

import nl.tudelft.rowapp.MainActivity;

public class AngleCalc {
	
	private boolean initialise = true;
	private int initialiseCount;
	private float[] angularVelocityHistory = new float[3];
	private float[] angleHistory = new float[3];
	
	
	public AngleCalc(){
		initialiseCount = 0;
		for(int i=0; i< angularVelocityHistory.length;i++){
			angularVelocityHistory[i] = 0;
		}
		for(int i=0; i< angleHistory.length;i++){
			angleHistory[i] = 0;
		}
	}
	
	
	/**
	 * this will shift all elements of the array 'angularVelocityHistory' to the right. The last element is removed.
	 * 'angularVelocity' is set to the first position of the array.
	 * @param angularVelocity is the new angular Velocity.
	 */
	private void fillAngularVelocityHistory(float angularVelocity){
		for(int end=angularVelocityHistory.length-1; end > 0; end--){
			angularVelocityHistory[end] = angularVelocityHistory[end - 1];
		}
		angularVelocityHistory[0] = angularVelocity;
	}
	
	/**
	 * this will shift all elements of the array 'angleHistory' to the right. The last element is removed.
	 * 'angle' is set to the first position of the array.
	 * @param 
	 */
	private void fillAngleHistory(float angle){
		angleHistory[2] = angleHistory[1];
		angleHistory[1] = angleHistory[0];
		angleHistory[0] = angle;
	}
	
	/**
	 * if oar is in drive direction this will return true.
	 * if oar is in recovery this will return false.
	 * @return
	 */
	public boolean driveDirection(){
		for(int i=0; i< angularVelocityHistory.length;i++){
			if(angularVelocityHistory[i]>=0){
				return true;
			}
		}
		return false;
	}

//	/**
//	 * DEZE IS VOOR DEBUG
//	 * @return
//	 */
//	public boolean driveDirection(){
//	for(int i=0; i< angularVelocityHistory.length;i++){
//		if(angularVelocityHistory[0]<0){
//			return false;
//		}
//	}
//	return true;
//}

	/**
	 * this function will return the Angular Velocity. The first two times this function is called the function will be initialize and will return the value 0. 
	 * @param angle
	 * @return
	 */
	public float getAngularVelocity(float angle){
		fillAngleHistory(angle);
		if(initialise){
			initialiseCount++;
			if(initialiseCount>1){
				initialise = false;
			}
			return 0;
		}
		float angularVelocity = (angleHistory[0] - angleHistory[2])/(2 * MainActivity.SAMPLE_TIME);
		fillAngularVelocityHistory(angularVelocity);
		return angularVelocity;
	}
	
	
}
