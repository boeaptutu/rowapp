package nl.tudelft.dataProcessing;

import nl.tudelft.rowapp.MainActivity;

public class PowerCalc {
	
	private final float distanceHandleToDoll = (float)0.80; //in meters
	private float totalPower = 0;
	private float totalForce = 0;
	
	
	/**
	 * will calculate the power and if the oar is in drive direction it will be added to the totalPower.
	 * Also the force will be added to the total force if the oar is in drive direction.
	 * @param force
	 * @param angularVelocity
	 * @param driveDirection
	 * @return power
	 */
	public float calculatePower(float force, float angularVelocity, boolean driveDirection){
		//times 1.7 so the second term is corrected.
		float power = (force * distanceHandleToDoll * angularVelocity)*1.143f;
		if(driveDirection){
			totalPower += power;
			totalForce += force;
		}
		return power;
	}
	
	/**
	 * will set totalPower and totalForce to zero. this function should be called at the end of a stroke.
	 */
	public void reset(){
		totalPower = 0;
		totalForce = 0;
	}
	
	/**
	 * 
	 * @return totalPower
	 */
	public float getTotalPower(){
		return totalPower;
	}
	
	/**
	 * 
	 * @return totalForce
	 */
	public float getTotalForce(){
		return totalForce;
	}

}
