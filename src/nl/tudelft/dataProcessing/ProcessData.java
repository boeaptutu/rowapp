package nl.tudelft.dataProcessing;

import java.util.concurrent.TimeUnit;

import android.util.Log;
import nl.tudelft.rowapp.MainActivity;

public class ProcessData {

	private AngleCalc angleCalc = new AngleCalc();
	private PowerCalc powerCalc  = new PowerCalc();
	
	private float totalPower = 0;
	private float strokePower = 0;
	private float[] strokePowerHistory = {0,0,0};
	
	private float totalForce = 0;
	private float strokeForce = 0;
	private float[] strokeForceHistory = {0,0,0};
	
	private boolean driveDirection = false;
	private boolean previousDriveDirection = false;
	private float[] SPMHistory = {0,0,0};
	
	private long start = System.nanoTime();
	private long sampleAmount = 0;
	private long strokeCounts = 0;
	
	private float bufferedForce = 0;
	
	
	
	/**
	 * function should be called every incoming sample so the complete stroke- force, power and SPM could be calculated.
	 * @param angle
	 * @param force
	 */
	public void calculateData(float angle, float force){
		//angularVelocity calculations
		float angularVelocity = angleCalc.getAngularVelocity(angle);
		driveDirection = angleCalc.driveDirection();
		
		sampleAmount++;
		
		//if oar direction is switched from recovery to drive the total power of one stroke will be available.
		if(previousDriveDirection != driveDirection && driveDirection == true){
			//total power calculations
			strokePower = powerCalc.getTotalPower()/sampleAmount;
			fillStrokePowerHistory(strokePower);
			totalPower += strokePower;
			
			strokeForce = powerCalc.getTotalForce()/sampleAmount;
			fillStrokeForceHistory(strokeForce);
			totalForce += strokeForce;
			
			//strokes per minute calculations.
			float StrokeTime = sampleAmount * MainActivity.SAMPLE_TIME_mS;
			fillSPMhistory(StrokeTime);
			sampleAmount = 0;
			
			
			powerCalc.reset();

		}
		
		previousDriveDirection = driveDirection;
		
		//current power calculation
		powerCalc.calculatePower(bufferedForce, angularVelocity, driveDirection);
		bufferedForce = force;
	}
	
	/**
	 * 
	 * @return average stroke power
	 */
	public float displayPower(){
		float averageStrokePower = 0;
		for(int i=0;i<strokePowerHistory.length; i++){
			averageStrokePower += strokePowerHistory[i]; 
		}
		averageStrokePower = averageStrokePower/strokePowerHistory.length;
		
		return averageStrokePower;
	}
	
	
	
	/**
	 * 
	 * @return average stroke force
	 */
	public float displayForce(){
		float averageStrokeForce = 0;
		for(int i=0;i<strokeForceHistory.length; i++){
			averageStrokeForce += strokeForceHistory[i]; 
		}
		averageStrokeForce = averageStrokeForce/strokeForceHistory.length;
		
		return averageStrokeForce;
	}
	
	
	public float getTotalPower(){
		return totalPower;
	}
	
	public float getTotalForce(){
		return totalForce;
	}
	
	
	/**
	 * will start the timer at the begin of the training
	 */
	public void startTimer(){
		start = System.nanoTime();
	}
	
	
	/**
	 * 
	 * @return total time in the String format: HH:MM:SS.
	 */
	public String DisplayTotalTime(){
		String Time = "";
		long elapsedTime = System.nanoTime() - start;
		long seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS)%60;
		long minutes = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS)%60;
		long hours = TimeUnit.HOURS.convert(elapsedTime, TimeUnit.NANOSECONDS);
		
		if(hours<10){
			Time += "0";
		}
		Time += hours+":";
		
		if(minutes<10){
			Time += "0";
		}
		Time += minutes+":";
		
		if(seconds<10){
			Time += "0";
		}
		Time += seconds;
		
		return Time;
	}
	
	/**
	 * this will shift all elements of the array 'SPMHistory' to the right. The last element is removed.
	 * 'StrokeTime' is set to the first position of the array.
	 * @param strokeTime
	 */
	private void fillSPMhistory(float strokeTime){
		for(int end=SPMHistory.length-1; end > 0; end--){
			SPMHistory[end] = SPMHistory[end - 1];
		}
		SPMHistory[0] = strokeTime;
	}
	
	/**
	 * this will shift all elements of the array 'strokePowerHistory' to the right. The last element is removed.
	 * 'strokePower' is set to the first position of the array.
	 * @param strokePower
	 */
	private void fillStrokePowerHistory(float strokePower){
		for(int end=strokePowerHistory.length-1; end > 0; end--){
			strokePowerHistory[end] = strokePowerHistory[end - 1];
		}
		strokePowerHistory[0] = strokePower;
	}
	
	/**
	 * this will shift all elements of the array 'strokeForceHistory' to the right. The last element is removed.
	 * 'strokeForce' is set to the first position of the array.
	 * @param strokeForce
	 */
	private void fillStrokeForceHistory(float strokeForce){
		for(int end=strokeForceHistory.length-1; end > 0; end--){
			strokeForceHistory[end] = strokeForceHistory[end - 1];
		}
		strokeForceHistory[0] = strokeForce;
	}
	
	/**
	 * @return strokes per minute
	 */
	public long displaySPM(){
		long averageStrokeTime = 0;
		for(int i=0;i<SPMHistory.length; i++){
			averageStrokeTime += SPMHistory[i]; 
		}
		averageStrokeTime = averageStrokeTime/SPMHistory.length;
		if(averageStrokeTime == 0){
			return 0;
		}
		// 60.000 milliseconds in one minute
		return 60000/averageStrokeTime;
	}
}
